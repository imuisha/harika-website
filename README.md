To build this project;

1) Download the repository from BitBucket

2) Make sure that you are using the latest version of yarn, node, axios nodemailer - for contact emails - and nextjs.

Note: You can also set up dotenv to pass your maps api key,  and gmail api key via an environment variable which you can set up to not be included in your commits.

Note: Generally do not stage or commit your yarn.lock file as it can cause build problems for others.

