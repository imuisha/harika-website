const Dotenv = require('dotenv-webpack');
const path = require('path');


module.exports = {
  node: {
    fs: 'empty',
    env: {
      MAPS_API_KEY: process.env.MAPS_API_KEY,
    },
  },

  webpack: (config, { isServer }) => {
    
    if (!isServer) {
      config.node = {
        fs: 'empty',
      };
    }

    // Read the .env file
    // new Dotenv({
    //   path: path.join(__dirname, '.env'),
    //   systemvars: true,
    // });

    return config;
  },
};
