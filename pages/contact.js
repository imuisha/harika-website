import React, { Component } from 'react';
import Link from 'next/link';
import Header from '../components/header';
import Footer from '../components/footer';
import MediaLinks from '../components/media-links';
import Clients from '../components/clients';

class Contact extends Component {
  render() {
    return (
      <div className='white'>
        <div>
          <Header />
        </div>

        <form
          className='contact'
          name='contact'
          action='/formSubmitted'
          method='POST'
          data-netlify='true'
          style={{ height: '700px' }}
        >
          <input type='hidden' name='form-name' value='contact' />
          <div style={{position:'relative', top: '100px' }}>
            <div
              className='page-text'
              style={{ position: 'relative', textAlign: 'center' }}
            >
              <h3
                style={{
                  position:'relative',
                  bottom: '25px',
                  marginLeft: '20px',
                  fontFamily: 'courier',
                  color: 'black',
                }}
              >
                Contact Us
              </h3>
            </div>
            <div className='container'>
              <div className='row'>
                <div className='input-field col s6'>
                  <label htmlFor='full-name'>Full Name</label>
                  <input
                    id='full-name'
                    name='full-name'
                    type='text'
                    className='validate'
                    style={{ color: 'white' }}
                  />
                </div>
                <div className='input-field col s6'>
                  <label htmlFor='email'>Email</label>
                  <input
                    id='email'
                    name='email'
                    type='text'
                    className='validate'
                    style={{ color: 'white' }}
                  />
                </div>
              </div>
              <div className='row'>
                <div className='input-field col s6'>
                  <input
                    id='organisation'
                    name='organisation'
                    type='text'
                    className='validate'
                    style={{ color: 'white' }}
                  />
                  <label htmlFor='organisation'>Organisation</label>
                </div>
                <div className='input-field col s6'>
                  <input
                    id='role'
                    name='role'
                    type='text'
                    className='validate'
                    style={{ color: 'white' }}
                  />
                  <label htmlFor='role'>Role</label>
                </div>
              </div>
              <div className='row'>
                <div className='input-field col s12'>
                  <textarea
                    id='enquiry'
                    name='enquiry'
                    className='materialize-textarea'
                    style={{ color: 'white' }}
                  ></textarea>
                  <label htmlFor='message'>Enquiry</label>
                </div>
              </div>
              <div className='file-field input-field'>
                <div className='btn deep-purple darken-2'>
                  <span>File</span>
                  <input type='file' name='files' id='files' multiple />
                </div>
                <div className='file-path-wrapper'>
                  <input
                    className='file-path validate'
                    type='text'
                    placeholder='Upload files (if applicable)'
                    color='white'
                  />
                </div>
              </div>
              <div
                className='row'
                style={{ position: 'relative', textAlign: 'center' }}
              >
                <button
                  className='btn waves-effect waves-light deep-purple darken-2'
                  type='submit'
                  name='action'
                >
                  Send
                  <i className='material-icons right'></i>
                </button>
              </div>
            </div>
          </div>
        </form>

        <div>
          <MediaLinks />
        </div>
        <div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default Contact;
