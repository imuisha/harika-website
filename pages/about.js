import React, { Component } from 'react';
import Link from 'next/link';
import Header from '../components/header';
import Footer from '../components/footer';
import MediaLinks from '../components/media-links';
import Typed from 'typed.js';

class About extends Component {
  render() {
    return (
      <div className='grey lighten-3'>
        <div>
          <Header />
        </div>

        <div
          className='main white'
          id='main'
          style={{
            position: 'relative',
            display: 'flex',
            overflow: 'scroll',
            height: '1000px',
            flexDirection: 'column',
          }}
        >
          <div className='container'>
            <div
              className='header-text'
              style={{ position: 'relative', textAlign: 'left' }}
            >
              <h3
                style={{
                  marginBottom: '0',
                  fontFamily: 'courier',
                  color: 'black',
                }}
              >
                About Us
              </h3>
            </div>
            <div className='row'>
              <h6 style={{ fontFamily: 'courier' }}>
                Harika Technology was founded in 2012 by Robin Kay and other
                partners. The founders gained the majority of their expertise
                through working in various roles at Citicorp, Qantas, Bunnings,
                and Jackson Mcdonald Lawyers to name a few. {'\r\n'} Robin in
                particular served as an executive for several ventures before
                deciding to build a consultancy service that covered the
                resources, engineering, and other commercial sectors that he had
                decades of experience in.
              </h6>
            </div>
            <div className='row' style={{ textAlign: 'center' }}>
              <img
                src='/images/perth.jpeg'
                style={{ width: '80%' }}
                alt='perth-image'
              ></img>
            </div>
            <div className='row'>
              <div
                className='col s12'
                style={{
                  position: 'relative',
                  top: '0px',
                  height: '50px',
                  border: 'solid',
                }}
              >
                <p style={{ textAlign: 'center' }}>
                  Users: xxxxx | Organisations: xx | Countries: xx | Data: xx
                </p>
              </div>
            </div>
          </div>
        </div>
        <div style={{ textAlign: 'right' }}>
          <Link href='/contact'>
            <a
              className='fixed-action-btn btn-large deep-purple darken-2'
            >
              Contact Us
            </a>
          </Link>
        </div>
        <div>
          <MediaLinks />
        </div>
        <div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default About;
