import App from 'next/app';
import React from 'react';
import 'materialize-css/dist/css/materialize.min.css';
import '../public/stylesheets/App.css';
import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react';


class MyApp extends App {
  componentDidMount() {
    const M = require('materialize-css');
    require('dotenv').config();
    M.AutoInit();
    document.addEventListener('DOMContentLoaded', function () {
      var btnElems = document.querySelectorAll('.fixed-action-btn');
      var btnInstances = M.FloatingActionButton.init(btnElems, options);

      var sliderElems = document.querySelectorAll('.slider');
      var sliderInstances = M.Slider.init(sliderElems, options);

      $('#enquiry').val('Enquiry');
      M.textareaAutoResize($('#enquiry'));

      if ($(window).height() > $('main').height()) {
        $('footer').css('position', 'fixed');
      } else {
        $('footer').css('position', 'static');
      }
    });
  }

  render() {
    const { Component, pageProps } = this.props;
    return <Component {...pageProps} />;
  }
}

export default MyApp;
