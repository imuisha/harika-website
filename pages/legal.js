import React, { Component } from 'react';
import Link from 'next/link';
import Header from '../components/header';
import Footer from '../components/footer';
import MediaLinks from '../components/media-links';

class Legal extends Component {
  render() {
    return (
      <div>
        <div className='body'>
          <div>
            <Header />
          </div>
          <div
            className='main white'
            id='main'
            style={{
              position: 'relative',
              display: 'flex',
              height: '400Px',
              flexDirection: 'column',
              marginBottom: '20px',
            }}
          >
            <div className='container'>
              <div className='row'>
                <div
                  clasName='col s12'
                  style={{ position: 'relative', top: '50px' }}
                >
                  <p>
                    Harika Technology respects your right to privacy and is
                    committed to safeguarding the privacy of our customers and
                    website visitors. This policy sets out how we collect and
                    treat your personal information. We adhere to the Australian
                    Privacy Principles contained in the Privacy Act 1988 (Cth)
                    and to the extent applicable, the EU General Data Protection
                    Regulation (GDPR). "Personal information" is information we
                    hold which is identifiable as being about you. This includes
                    information such as your name, email address, identification
                    number, or any other type of information that can reasonably
                    identify an individual, either directly or indirectly. You
                    may contact us in writing at 221 St Georges Terrace, Perth,
                    Western Australia, 6000 for further information about the
                    below Privacy Policy.
                  </p>
                </div>
              </div>
              <div className='row'>
                <div
                  className='col s12'
                  style={{
                    position: 'relative',
                    textAlign: 'center',
                    top: '100px',
                  }}
                >
                  <a
                    className='waves-effect waves-light btn-large deep-purple darken-2'
                    href='/docs/gdpr-privacy-policy.pdf'
                    target='_blank'
                  >
                    View our privacy policy
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default Legal;
