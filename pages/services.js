import React, { Component } from 'react';
import Link from 'next/link';
import Header from '../components/header';
import Footer from '../components/footer';
import MediaLinks from '../components/media-links';
import Clients from '../components/clients';

class Services extends Component {
  render() {
    return (
      <div>
        <div className='body'>
          <div>
            <Header />
          </div>
          <div
            className='main white'
            id='main'
            style={{
              position: 'relative',
              display: 'flex',
              height: '2000x',
              flexDirection: 'column',
              marginBottom:'20px'
            }}
          >
            <div className='container'>
              <div className='header'>
                <h3 style={{ color: 'black' }}>Services</h3>
              </div>

              <div className='row'>
                <div class='col s6 m3'>
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <h4 style={{ color: 'black' }}>Web Applications</h4>
                    <span class='black-text'>
                      A website makes or breaks the first impression of your
                      business. A web application can be a website, or a patent
                      technology driven software suite for your customers. It is
                      the majority or entire business footprint for a lot of
                      companies and we have the highest level of technical
                      competency in planning and delivering these projects.
                    </span>
                  </div>
                </div>
                <div class='col s6 m3'>
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <h4 style={{ color: 'black' }}>Mobile Applications</h4>
                    <span class='black-text'>
                      61.67% of the world's population are smartphone users. As
                      this number continues to grow, the need for a strong
                      digital and if sensible mobile presence is essential.
                      Viable businesses make themselves availale to consumers at
                      the consumer's convenience, and a dedicated mobile
                      presence is an integral part of this accessibility.
                    </span>
                  </div>
                </div>
                <div class='col s6 m3'>
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <h4 style={{ color: 'black' }}>Databases</h4>
                    <span class='black-text'>
                      A good database follows the CRUD principle. That means
                      that it allows you to create, read, update and delete data
                      as you see fit in a reliable and predicatable manner.
                      Database integrity is one of the largest software related
                      challenges for modern businesses and it is essential to
                      use the services of an experienced and reputable company
                      to build, revise, and maintain your database.
                    </span>
                  </div>
                </div>
                <div class='col s4 m3'>
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <h4 style={{ color: 'black' }}>Integrated Systems</h4>
                    <span class='black-text'>
                      For businesses whose success is driven by IP and patent
                      technology, custom built internal and user software is
                      essential. Whether it is purely software based or
                      integrating with hardware.
                    </span>
                  </div>
                </div>
              </div>
              <div className='row'>
                <div class='col s4 m3'>
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <h4 style={{ color: 'black' }}>Cloud Infastructure</h4>
                    <span class='black-text'>
                      In a world with an ever growing digital economy, and
                      ever-expanding databases, reliable, and efficient cloud
                      infastructure is essential. Using the latest technologies,
                      we can provide this for you.
                    </span>
                  </div>
                </div>
                <div class='col s4 m3'>
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <h4 style={{ color: 'black' }}>DevOps</h4>
                    <span class='black-text'>
                      For the large scale or aspiring enterprises, automation
                      becomes a necessity with scale. Whether your needs are
                      purely software based or integrate hardware, we can
                      deliver a consistent and powerful backend for your company
                      for a seamless user experience through our iterative
                      development processess.
                    </span>
                  </div>
                </div>
                <div class='col s4 m3'>
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <h4 style={{ color: 'black' }}>Web Migration</h4>
                    <span class='black-text'>
                      When you need to rebrand or renovate your online
                      infastructure, we can help make the process as seemless as
                      possible and protect your data integrity throughout the
                      process.
                    </span>
                  </div>
                </div>
                <div class='col s4 m3'>
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <h4 style={{ color: 'black' }}>Development Audits</h4>
                    <span class='black-text'>
                      As an experienced software development company, we have
                      the technical expertise to audit the work of other
                      consultancies to provide you with the peace of mind that
                      you need regarding the security, scalability, and
                      commercial viability of your software product(s).
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default Services;
