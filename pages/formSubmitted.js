import React, { Component } from 'react';
import Link from 'next/link';
import Header from '../components/header';
import Footer from '../components/footer';
import MediaLinks from '../components/media-links';
import Clients from '../components/clients';

export default function formSubmitted() {
  return (
    <div className='theme'>
      <div
        className='body'
        style={{
          height: '600px',
          position: 'relative',
          overflow: 'scroll',
          marginBottom: '',
        }}
      >
        <div>
          <Header />
        </div>
        <div className='container'>
          <div className='row'>
            <div
              style={{
                position: 'relative',
                top: '200px',
                textAlign: 'center',
              }}
            >
              <div>
                <h3 style={{ fontFamily: 'courier', color: 'white' }}>
                  Your enquiry has been submitted
                </h3>
              </div>
            </div>
          </div>
          <div className='row'>
            <div
              style={{
                position: 'relative',
                top: '200px',
                textAlign: 'center',
              }}
            >
              <Link href='/index'>
                <a className='waves-effect waves-light btn deep-purple darken-2'>
                  Return Home
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div>
        <Clients />
        <Footer />
      </div>
    </div>
  );
}
