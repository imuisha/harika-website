import React, { Component } from 'react';
import Typed from 'typed.js';
import Link from 'next/link';
import Header from '../components/header';
import Footer from '../components/footer';
import MediaLinks from '../components/media-links';
import Clients from '../components/clients';

class Index extends Component {
  componentDidMount() {
    const words = [
      'Your source of software expertise',
      'Web applications',
      'Mobile applications',
      'Databases',
      'Integrated systems',
      'Cloud infastructure',
      'DevOps',
      'Web migration',
      'Development Audits',
    ];

    const options = {
      strings: words,
      typeSpeed: 40,
      backSpeed: 0,
      loop: true,
      cursorChar: '',
    };

    this.typed = new Typed(this.Element, options);
  }

  componentWillUnmount() {
    this.typed.destroy();
  }

  render() {
    return (
      <div>
        <div className='body'>
          <div>
            <Header />
          </div>

          <div
            className='main white'
            id='main'
            style={{
              position: 'relative',
              display: 'flex',
              height: '600px',
              flexDirection: 'column',
            }}
          >
            <div className='container'>
              <div className='row'>
                <div className='text-container'>
                  <div
                    className='landing-text col s12'
                    style={{
                      position: 'relative',
                      textAlign: 'center',
                      top: '50px',
                    }}
                  >
                    <h3
                      style={{
                        fontFamily: 'courier',
                        color: 'black',
                        whiteSpace: 'pre',
                        height: '50px',
                        padding: '10px',
                      }}
                      ref={(Element) => {
                        this.Element = Element;
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className='row'>
                <div
                  className='col s4'
                  style={{
                    position: 'relative',
                    top: '102px',
                    height: '240px',
                  }}
                >
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <img
                      src='/images/collab3.png'
                      style={{
                        position: 'relative',
                        width: '20%',
                        top: '10px',
                        left: '120px',
                      }}
                    ></img>
                    <p style={{ color: 'black' }}>
                      We work with corporations, government organisations, SMEs,
                      and non-for-profits with a focus on delivery, and
                      consistent performance. We aim to ensure that our
                      technology is scalable, and able to cope with dynamic
                      business requirements.
                    </p>
                  </div>
                </div>
                <div
                  className='col s4'
                  style={{
                    position: 'relative',
                    top: '70px',
                    height: '240px',
                  }}
                >
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <img
                      src='/images/data_analytics.gif'
                      style={{
                        position: 'relative',
                        width: '18%',
                        top: '20px',
                        left: '120px',
                      }}
                    ></img>
                    <p style={{ color: 'black' }}>
                      Integral business processes and components require the
                      greatest technical application available. At Harika, our
                      work is client-focused and tailored to suit your business
                      needs.
                    </p>
                  </div>
                </div>
                <div
                  className='col s4'
                  style={{
                    position: 'relative',
                    top: '100px',
                    height: '240px',
                  }}
                >
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <img
                      src='/images/cloud3.png'
                      style={{
                        position: 'relative',
                        width: '22%',
                        top: '20px',
                        left: '120px',
                      }}
                    ></img>
                    <p style={{ color: 'black' }}>
                      Technology is crucial to business success. It is essential
                      that such a core business element is built and developed
                      by highly technical and experienced teams. That is what we
                      offer at Harika Technology.
                    </p>
                  </div>
                </div>
                <div className='col s12' style={{ textAlign: 'center' }}>
                  <Link href='/about'>
                    <a
                      className='btn-large waves-effect waves-light deep-purple darken-2'
                      style={{ top: '130px' }}
                    >
                      Learn More
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div
            className='white'
            style={{ height: '600px', marginBottom: '40px' }}
          >
            <div className='container'>
              <div className='row'>
                <div
                  className='col s12'
                  style={{
                    position: 'relative',
                    height: '5px',
                  }}
                >
                  <h4 style={{ color: 'black' }}>Our Clients</h4>
                </div>
              </div>

              <div className='row' style={{ position: 'relative' }}>
                <div
                  className='col s3'
                  style={{
                    position: 'relative',
                    top: '60px',
                    height: '420px',
                    margin: '1px',
                  }}
                >
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <a href='//www.globaltech.com.au/' target='blank'>
                      <img
                        src='/images/gt-site.png'
                        style={{
                          position: 'relative',
                          width: '107%',
                          top: '0px',
                          right: '11px',
                        }}
                      ></img>
                    </a>
                    <h4 style={{ color: 'black' }}>Globaltech</h4>
                    <p style={{ color: 'black' }}>
                      GlobalTech are the Australia based subsidiary of Boart
                      Longyear, a U.S based global mineral exploration company.
                      Our work with the conglomerate covers software integrated
                      exploration tools and applications and analysis software.
                    </p>
                  </div>
                </div>
                <div
                  className='col s3'
                  style={{
                    position: 'relative',
                    top: '50px',
                    height: '420px',
                    margin: '1px',
                  }}
                >
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <a href='//www.prok.com/' target='blank'>
                      <img
                        src='/images/prok-site.png'
                        style={{
                          position: 'relative',
                          width: '107%',
                          top: '0.5px',
                          right: '11px',
                        }}
                      ></img>
                    </a>
                    <h4 style={{ color: 'black' }}>Prok</h4>
                    <p style={{ color: 'black' }}>
                      Prok is a global conveyer equipment manufacturer. Prok
                      delivers 2 million rollers and 3000 pulleys to clients on
                      an annual basis. We contribute the software capabilities
                      that help the company design these components in
                      collaboration with their in house enginnering and design
                      teams.
                    </p>
                  </div>
                </div>
                <div
                  className='col s3'
                  style={{
                    position: 'relative',
                    top: '60px',
                    height: '420px',
                    margin: '1px',
                  }}
                >
                  <div class='card-panel' style={{ backgroundColor: 'white' }}>
                    <a href='//www.raswa.org.au/' target='blank'>
                      <img
                        src='/images/raswa-site.png'
                        style={{
                          position: 'relative',
                          width: '107%',
                          top: '0px',
                          right: '11px',
                        }}
                      ></img>
                    </a>
                    <h4 style={{ color: 'black' }}>RASWA</h4>
                    <p style={{ color: 'black' }}>
                      The Royal Agricultural Society of Western Australia
                      (RASWA) is a member-based NPO that works to promote the
                      importance of rural Western Australia and its primary
                      industries. We manage the organisation's website and
                      extensive database.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div>
          <MediaLinks />
        </div>

        <div>
          <Footer />
          <div style={{ textAlign: 'right' }}>
            <Link href='/contact'>
              <a className='fixed-action-btn btn-large deep-purple darken-2'>
                Contact Us
              </a>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Index;
