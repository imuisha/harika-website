import React, { Component } from 'react';
import Link from 'next/link';

class MediaLinks extends Component {
  render() {
    return (
      <div>
        <div
          className='fixed-action-btn'
          style={{ left: '20px', bottom: '15px' }}
        >
          <Link href='//linkedin.com/company/harika-technology'>
            <a className='linkedInLogo' target='_blank'>
              <img src='/images/LI-In-Bug.png' style={{ width: '4%' }}></img>
            </a>
          </Link>
        </div>

        <div
          className='fixed-action-btn'
          style={{ left: '100px', bottom: '0px' }}
        >
          <Link href='//twitter.com/harikatechno'>
            <a className='twitterLogo' target='_blank'>
              <img
                src='/images/Twitter_Logo_Blue.png'
                style={{ width: '6%' }}
              ></img>
            </a>
          </Link>
        </div>
      </div>
    );
  }
}

export default MediaLinks;
