import React, { Component } from 'react';
import Link from 'next/link';

class Clients extends Component {
  render() {
    return (
      <div>
        <div className='container'>
          <div className='row'>
            <h5
              style={{
                marginBottom: '0',
                fontFamily: 'courier',
                color: 'black',
              }}
            >
              Our Clients
            </h5>
          </div>
          <div className='row' style={{ marginBottom: '20px' }}>
            <div className='partner col s4'>
              {' '}
              <img src='/images/gt.jpg'></img>
            </div>
            <div className='partner col s4'>
              {' '}
              <img src='/images/prok.png' style={{ width: '75%' }}></img>
            </div>
            <div className='partner col s4'>
              {' '}
              <img src='/images/raswa.png' style={{ scale: '50%' }}></img>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Clients;
