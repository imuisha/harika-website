import React, { Component } from 'react';
import Link from 'next/link';

class Header extends Component {
  render() {
    return (
      <div className='navbar-fixed'>
        <nav>
          <div className='nav-wrapper light-blue'>
            <a href='/index' className='brand-logo'>
              <img
                src='images/Harika-Technology.png'
                alt='Harika Technology'
                style={{ width: '25%', marginLeft: '20px', marginTop: '10px' }}
              ></img>
            </a>
            <ul
              id='nav-mobile'
              className='right hide-on-med-and-down'
              style={{ position: 'fixed', right: '20px' }}
            >
              <li>
                <Link href='/index'>
                  <a
                    style={{
                      color: 'white',
                      fontFamily: 'courier-new-header',
                      fontSize: 'large',
                    }}
                  >
                    Home
                  </a>
                </Link>
              </li>
              <li>
                <Link href='/about'>
                  <a
                    style={{
                      color: 'white',
                      fontFamily: 'courier-new-header',
                      fontSize: 'large',
                    }}
                  >
                    About
                  </a>
                </Link>
              </li>
              <li>
                <Link href='/services'>
                  <a
                    style={{
                      color: 'white',
                      fontFamily: 'courier-new-header',
                      fontSize: 'large',
                    }}
                  >
                    Services
                  </a>
                </Link>
              </li>
              <li>
                <Link href='/contact'>
                  <a
                    style={{
                      color: 'white',
                      fontFamily: 'courier-new-header',
                      fontSize: 'large',
                    }}
                  >
                    Contact
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default Header;
