import React, { Component } from 'react';
import Link from 'next/link';

class Footer extends Component {
  render() {
    return (
      <div
        className='footer light-blue'
        style={{
          position: 'relative',
          overflow: 'scroll',
          bottom: '0',
          marginTop: 'auto',
          height: '420px',
          width: 'auto',
        }}
      >
        <div className='container'>
          <div className='row' style={{marginTop:'20px'}}>
            <div className='col s4'>
              <h5 style={{color:'white'}}>Navigate</h5>
            </div>
            <div className='col s4'>
              <h5 style={{color:'white'}}>Services</h5>
            </div>
            <div className='col s4'>
              <h5 style={{color:'white'}}>Contact Us</h5>
            </div>
          </div>
          <div className='row'>
            <div className='col s4'>
              <Link href='/index'>
                <a style={{ color: 'white' }}>Home</a>
              </Link>
            </div>
            <div className='col s4'>
              <Link href='/services'>
                <a style={{ color: 'white' }}>Web Applications</a>
              </Link>
            </div>
            <div className='col s4'>
              <Link href='/contact'>
                <a style={{ color: 'white' }}>Contact</a>
              </Link>
            </div>
          </div>
          <div className='row'>
            <div className='col s4'>
              <Link href='/services'>
                <a style={{ color: 'white' }}>Services</a>
              </Link>
            </div>
            <div className='col s4'>
              <Link href='/about'>
                <a style={{ color: 'white' }}>Mobile Applications</a>
              </Link>
            </div>
            <div className='col s4'>
              <Link href='//linkedin.com/company/harika-technology'>
                <a style={{ color: 'white' }}>LinkedIn</a>
              </Link>
            </div>
          </div>
          <div className='row'>
            <div className='col s4'>
              <Link href='/about'>
                <a style={{ color: 'white' }}>About</a>
              </Link>
            </div>
            <div className='col s4'>
              <Link href='/services'>
                <a style={{ color: 'white' }}>Databases</a>
              </Link>
            </div>
            <div className='col s4'>
              <Link href='//twitter.com/harikatechno'>
                <a style={{ color: 'white' }}>Twitter</a>
              </Link>
            </div>
          </div>
          <div className='row'>
            <div className='col s4'>
              <Link href='/legal'>
                <a style={{ color: 'white' }}>Legal</a>
              </Link>
            </div>
            <div className='col s4'>
              <Link href='/services'>
                <a style={{ color: 'white' }}>Integrated Systems</a>
              </Link>
            </div>
            <div className='col s4'>
              <a style={{ color: 'white' }} href='tel: 1300 427 452'>
                1300 HARIKA
              </a>
            </div>
          </div>
          <div className='row'>
            <div className='col s4'>
              <Link href='#'>
                <a style={{ color: 'white' }}></a>
              </Link>
            </div>
            <div className='col s4'>
              <Link href='/services'>
                <a style={{ color: 'white' }}>Cloud Infastructure</a>
              </Link>
            </div>
            <div className='col s4'>
              <a style={{ color: 'white' }} href='mailto:info@harika.com.au'>
                info@harika.com.au
              </a>
            </div>
          </div>
          <div style={{ textAlign: 'center', padding:'40px' }}>
            <h6 className='white-text' style={{ position: 'relative' }}>
              © Harika Technology 2012-2020 | ABN: 99 601 708 807{' '}
            </h6>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
